# Podio

An audio player (pure javascript). Pronounced Pod-ioh, like Audio. It was a dumb
name choice.

Huge attribution to Alexander Katz ([http://alexkatz.me](http://alexkatz.me)),
as I basically had no experience with html5 audio before checking out his
[codepen](https://codepen.io/katzkode/pen/ZbxYYG).


## Usage
Put a div somewhere that looks like this:

```html
<div id="player" data-src="/some/audio/file" data-img="/some/image/file.png">
</div>
```

**NOTE:** The data-src attribute *doesn't* have a file extension. The player is
going to automatically load both .mp3 and .ogg extensions.

Next just load up the podio.js and initialize

```html
<script src="podio.js"></script>
<script>
    Podio(document.getElementById("player"))
</script>
```

You should be able to load up as many players as you want!

Ideally you should use some CSS to make the player look respectable.