function Podio(element){
  var self = this
  self.duration = null
  self.image = null
  self.audio_element = null
  self.audio_player = null
  self.components = {
  	playbutton: null,
    playhead: null,
    timeline: null,
    title: null,
    caption: null,
    currenttime: null,
    finaltime: null,
  }
  self.duration = 1
  self.timeline_width = 1
  self.play_text = "Play Audio"
  self.pause_text = "Pause Audio"
  // Helpers
  var computeTime = function(val){
  	if (isNaN(val)){
    	return '0:00'
    }
    var mins = '' + Math.floor(val / 60) + ':'
    var secs = '0' + Math.floor(val % 60)
    return mins + secs.slice(-2)
  }
  
  // Control Functions
  var ctrl_play = function(){
  	self.audio_element.play()
    self.components.playbutton.innerHTML =
    	self.pause_text
  }
  var ctrl_pause = function(){
  	self.audio_element.pause()
    self.components.playbutton.innerHTML =
    	self.play_text
  }
  
  var ctrl_clickPlay = function(){
  	if (self.audio_element.paused){
    	ctrl_play()
    }
    else(
    	ctrl_pause()
    )
  }
  
  var ctrl_updateTime = function(){
  	var p = (
    	self.audio_element.currentTime /
      self.audio_element.duration
    ) * 100
    self.components.playhead.style.width = ''+ p + '%'
    self.components.currenttime.nodeValue = computeTime(
    	self.audio_element.currentTime
    )
  }
  
  var ctrl_clickTimeline = function(event){
  	var rect = self.components.timeline
    	.getBoundingClientRect()
    var click = event.clientX
    self.audio_element.currentTime = (
    	(click - rect.left) / rect.width
    ) * self.audio_element.duration
  }
  
  
  // Creation Functions
  var create_audioElement = function(){
		var audio_element = document.createElement('AUDIO')
  	audio_element.setAttribute('style', 'display: none;')
    var sourcemp3 = document.createElement('SOURCE')
    sourcemp3.setAttribute('src', element.getAttribute('data-src') + '.mp3')
    var sourceogg = document.createElement('SOURCE')
    sourceogg.setAttribute('src', element.getAttribute('data-src') + '.ogg')
    
    audio_element.appendChild(sourcemp3)
    audio_element.appendChild(sourceogg)
    element.appendChild(audio_element)
    self.audio_element = audio_element
  }
  var create_audioPlayer = function(){
 		var player_element = document.createElement('DIV')
    player_element.setAttribute('class', 'podio-player')
    
    self.image = element.getAttribute('data-img')
    var artwork = document.createElement('DIV')
    artwork.setAttribute('class', 'artwork')
    artwork.style.backgroundImage = 'url("'+ self.image +'")'
    player_element.appendChild(artwork)
        
    var timeline = document.createElement('DIV')
    timeline.setAttribute('class', 'timeline')
    self.components.timeline = timeline
    player_element.appendChild(timeline)
    
    var playhead = document.createElement('DIV')
    playhead.setAttribute('class', 'playhead')
    playhead.setAttribute('style', 'width: 0;')
    self.components.playhead = playhead
    timeline.appendChild(playhead)
    
    var controls = document.createElement('DIV')
    controls.setAttribute('class', 'controls')
    player_element.appendChild(controls)
    
    var times = document.createElement('DIV')
    times.setAttribute('class', 'times')
    controls.appendChild(times)
    
    var title = document.createElement('DIV')
    title.setAttribute('class', 'podio-title')
    title.innerHTML = element.getAttribute('data-title')
    controls.appendChild(title)
    
    var caption = document.createElement('DIV')
    caption.setAttribute('class', 'podio-caption')
    caption.innerHTML = element.getAttribute('data-caption')
    controls.appendChild(caption)
    
    
    var currenttime_div = document.createElement('DIV')
    var finaltime_div = document.createElement('DIV')
    var currenttime = document.createTextNode('0:00')
    var finaltime = document.createTextNode('0:00')
    self.components.currenttime = currenttime
    self.components.finaltime = finaltime
    currenttime_div.appendChild(currenttime)
    finaltime_div.appendChild(finaltime)

		var playbutton = document.createElement('DIV')
    playbutton.setAttribute('class', 'playbutton')
    playbutton.innerHTML = self.play_text
    self.components.playbutton=playbutton
    times.appendChild(currenttime_div)
    times.appendChild(playbutton)
    times.appendChild(finaltime_div)
    
		//var playbutton = document.createElement('BUTTON')
    //var playbutton_text = document.createTextNode('play')
    //playbutton.appendChild(playbutton_text)
    //self.components.playbutton = playbutton
    //controls.appendChild(playbutton)

		element.appendChild(player_element)
    self.audio_player = player_element
  }
  
  var bindEverything = function(){
  	self.components.playbutton.addEventListener(
    	'click', ctrl_clickPlay, false
    )
    self.components.timeline.addEventListener(
    	'click', ctrl_clickTimeline, false
    )
    self.audio_element.addEventListener(
    	'durationchange',
      function(){
      	self.duration = this.duration
        self.components.finaltime.nodeValue = computeTime(
        	this.duration
        )
       }
    )
    self.audio_element.addEventListener(
    	'timeupdate',
      ctrl_updateTime,
      false
    )
  }
  
  // Init that shit
  
  create_audioElement()
  create_audioPlayer()
  bindEverything()
  return self
}

window.Podio = Podio;